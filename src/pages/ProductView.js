import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView(){

	const { user } = useContext(UserContext);
	const { productId } = useParams();
	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [order, setOrder] = useState([]);

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId])

	const usercheckout = (productId) =>{
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log("Order data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successful",
					icon: "success",
					text: "Thank you for buying!."
				});

				navigate("/product");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});
	
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{
								(user.id !== null)
								?
									<Button variant="primary" size="lg" onClick={() => usercheckout(productId)}>Buy Now</Button>
								:
									<Button as={Link} to="/login" variant="success"  size="lg">Shop now</Button>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}