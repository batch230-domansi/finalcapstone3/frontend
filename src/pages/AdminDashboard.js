import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	const [allProduct, setAllProduct] = useState([]);
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const openAdd = () => setShowAdd(true); 
	const closeAdd = () => setShowAdd(false);
	const [showEdit, setShowEdit,setOrder] = useState(false);
	const [showOrder, setShowOrder] = useState(false);
	const openOrder = () => setShowOrder(true); 
	const closeOrder = () => setShowOrder(false);
	const [order] = useState([]);


	const openEdit = (id) => {
		setProductId(id);

		fetch(`${ process.env.REACT_APP_API_URL }/product/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock);
		});

		setShowEdit(true)
	};


	const closeEdit = () => {

	    setName('');
	    setDescription('');
	    setPrice(0);
	    setStock(0);
		setShowEdit(false);
	};



	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/product/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setAllProduct(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	useEffect(()=>{
		fetchData();
		fetchOrder();
	}, [])


	const archive = (id, productName) =>{
		// console.log(id);
		// console.log(productName);
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const addProduct = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/product/addProduct`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    stock: stock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});
		    		fetchData();
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStock(0);
	}



	const editProduct = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    stock: stock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});
		    		fetchData();
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStock(0);
	} 

	useEffect(() => {

        if(name != "" && description != "" && price > 0 && stock > 0){
            setIsActive(true);
        } 
        else {
            setIsActive(false);
        }

	}, [name, description, price, stock]);

	const fetchOrder = () => {
  fetch(`${process.env.REACT_APP_API_URL}/order`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`,
    },
  })
  .then((res) => res.json())
  .then((data) => {
    setOrder(data);
  })
  .catch((error) => {
    console.error("Error fetching orders:", error);
  });
};

						return(
								(userRole)?
								<>
									{}
									<div className="mt-5 mb-3 text-center">
										<h1>Admin Dashboard</h1>
										<Button variant="success" className="
										mx-2" onClick={openAdd}>Add Product</Button>
										<Button variant="success" className="mx-2" onClick={openOrder}>
											  Show Order
											</Button>
									</div>
									{}
									{}
									<Table striped bordered hover>
								      <thead>
								        <tr>
								          <th>Product ID</th>
								          <th>Product Name</th>
								          <th>Description</th>
								          <th>Price</th>
								          <th>Stock</th>
								          <th>Status</th>
								          <th>Actions</th>
								        </tr>
								      </thead>
								      <tbody>
							        	{allProduct}
								      </tbody>
								    </Table>
									{}


	    	{}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stock" className="mb-3">
	    	                <Form.Label>Product Stocks</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Stocks" 
	    		                value = {stock}
	    		                onChange={e => setStock(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    			</Modal.Body>
	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>
	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

					{}
					<Modal show={showOrder} fullscreen={true} onHide={closeOrder}>
					  <Modal.Header closeButton>
					    <Modal.Title>Order status</Modal.Title>
					  </Modal.Header>

					  <Modal.Body>
					    <Table striped bordered hover>
					      <thead>
					        <tr>
					          <th>Customer ID</th>
					          <th>Customer Name</th>
					          <th>Product Name</th>
					          <th>Amount to Pay</th>
					        </tr>
					      </thead>
					      <tbody>
					        {order.map((order) => (
					          <tr key={order._id}>
					            <td>{order.usersId}</td>
					            <td>{order.users}</td>
					            <td>{order.productName}</td>
					            <td>{order.amountToPay}</td>
					          </tr>
					        ))}
					      </tbody>
					    </Table>
					  </Modal.Body>
					  <Modal.Footer>
					    <Button variant="secondary" onClick={closeOrder}>
					      Close
					    </Button>
					  </Modal.Footer>
					</Modal>
					{}
    	{/*Modal for Editing a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="stock" className="mb-3">
    	                <Form.Label>Product Stock</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Stock" 
    		                value = {stock}
    		                onChange={e => setStock(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
             </Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

	    	</Form>	
	    </Modal>
    </>	
		:

		<Navigate to="/product" />
	)

	
}