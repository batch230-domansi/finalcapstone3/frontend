import {Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react'; //
import UserContext from "../UserContext"
import logo from '../logo.png'; 

export default function AppNavbar(){
	
	const { user } = useContext(UserContext);
	console.log(user);

	return(
		<Navbar bg="light" expand="lg" className="align-items-center">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">
		        <img
		            src={logo}
		            width="150"
		            height="150"
		            className="d-inline-block align-top mr-2"
		            alt="Your logo"
		          />
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            <Nav.Link as={NavLink} to="/product" >Catalog</Nav.Link>
			            {(user.id !== null)? // s55
			            	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
					            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
				            </Fragment>				            
			 
			        	}
			    
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}