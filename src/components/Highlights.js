
import { Row, Col, Card } from 'react-bootstrap';
export default function Highlights() {
    return (
        

        <Row className="mt-3 mb-3">
        <div></div>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>"Enjoy Up to 50% Off on Our Best-Selling Shirts - Limited Time Only!"</h2>
                        </Card.Title>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>What Our Customers Are Saying</h2>
                        </Card.Title>
                        <Card.Text>
                            <p>Name: Sarah K.</p>
                            <p>Star Rating: ★★★★★</p>
                            <p>Quote: "I absolutely love my new graphic tee from [Your Brand]! The design is unique, the fabric is so soft, and the fit is perfect. I've received so many compliments already. Can't wait to buy more!"</p>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Share Your Style: Become Part of Our Fashion Family</h2>
                        </Card.Title>
                        <Card.Text>
                            <p>Share Your Unique Look on Instagram, and You Could Win a 500PHP cash</p>

                            <p>To Join , post a photo wearing a shirt from our store, tag @Lassie Raiment, and use the hashtag #TeeTogether. One winner will be chosen monthly to receive a 500PHP cash.</p>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
