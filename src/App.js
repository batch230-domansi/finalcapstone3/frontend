// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/ // s53 to comment
import { BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; //s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Product from './pages/Product';
import ProductView from './pages/ProductView'; // s55
import AdminDashboard from './pages/AdminDashboard';

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'; 
import Error from './pages/Error'; 


// s54 Additional Example
import Settings from './pages/Settings'; 

// s54
import { UserProvider } from './UserContext';

// s55
import { useState, useEffect, useContext } from 'react';

function App() {


  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
    // setUser({
    //         id:null,
    //         isAdmin: null
    //     });
  }
        

      useEffect (() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                  headers: {
                      Authorization: `Bearer ${localStorage.getItem('token')}`
                      }
                  })
                  .then(res => res.json())
                  .then(data => {

                    if (typeof data._id !== undefined){
                       setUser({
                          id: data._id,
                          isAdmin: data.isAdmin
                      })
                    }
                     else{
                      setUser({
                          id:null,
                          isAdmin: null
                      });
                 }
              })
        }, [])

  return (
    /* React Fragments allows us to return multiple elements*/

    // 2 - provide/share the state to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product" element={<Product />} />
            <Route path="/admin" element={<AdminDashboard />} />
            <Route path="/product/:productId" element={<ProductView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} /> 
            <Route path="/settings" element={<Settings />} /> 
            <Route path="*" element={<Error />} /> 
        
          </Routes>
        </Container>
      </ Router>
    </ UserProvider>
    
  );
}


export default App;
